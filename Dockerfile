FROM nginx:1.15
#将dist目录拷贝到/etc/nginx/html中
COPY dist /etc/nginx/html
#将nginx配置文件目录拷贝到/etc/nginx中
COPY conf /etc/nginx